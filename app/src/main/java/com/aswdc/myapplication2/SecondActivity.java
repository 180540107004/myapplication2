package com.aswdc.myapplication2;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aswdc.myapplication2.adapter.UserListAdapter;
import com.aswdc.myapplication2.util.Const;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class SecondActivity extends AppCompatActivity {

    ListView lvUsers;
    Spinner spUsers;
    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();
    UserListAdapter userListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        initViewReferance();
        bindViewValues();
    }

    void bindViewValues() {

        userList.addAll((Collection<? extends HashMap<String, Object>>) getIntent().getSerializableExtra("UserList"));
        userListAdapter = new UserListAdapter(this, userList);
        lvUsers.setAdapter(userListAdapter);
        spUsers.setAdapter(userListAdapter);

        lvUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long itemId) {
                Toast.makeText(SecondActivity.this,userList.get(position).get(Const.GENDER).toString(),Toast.LENGTH_SHORT).show();
            }
        });
        spUsers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Toast.makeText(SecondActivity.this,userList.get(position).get(Const.GENDER).toString(),Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    void initViewReferance() {
        lvUsers = findViewById(R.id.lvActUserList);
        spUsers = findViewById(R.id.spActUsers);
    }

}