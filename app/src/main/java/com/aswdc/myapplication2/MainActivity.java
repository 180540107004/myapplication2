package com.aswdc.myapplication2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.aswdc.myapplication2.database.MyDatabase;
import com.aswdc.myapplication2.database.TblUserData;
import com.aswdc.myapplication2.util.Const;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    EditText etFirstName, etPhoneNumber, etEmailAddress;
    //EditText etLastName;
    Button btnSubmit;
    TextView tvDisplay;
    ImageView ivClose;

    RadioGroup rgGender;
    RadioButton rbMale;
    RadioButton rbFemale;
    CheckBox chbCricket;
    CheckBox chbFootball;
    CheckBox chbHockey;

    ImageView ivName;
    ImageView ivPhone;
    ImageView ivEmail;

    long phoneInteger;

    ArrayList<HashMap<String, Object>> userList = new ArrayList<>();

    long back_pressed;

    @Override
    public void onBackPressed() {
        if (back_pressed + 1000 > System.currentTimeMillis()) {
            super.onBackPressed();
        } else {
            Toast.makeText(getBaseContext(),
                    "Press once again to exit!", Toast.LENGTH_SHORT)
                    .show();
        }
        back_pressed = System.currentTimeMillis();
    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("TextViewValue", tvDisplay.getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewReferance();
        intViewEvent();
        setTypefaceOnView();
    }

    void initViewReferance() {
        new MyDatabase(MainActivity.this).getReadableDatabase();

        etFirstName = findViewById(R.id.etActFirstName);
        etPhoneNumber = findViewById(R.id.etActPhoneNumber);
        etEmailAddress = findViewById(R.id.etActEmail);
        // etLastName = findViewById(R.id.etActLastName);
        btnSubmit = findViewById(R.id.etActSubmit);
        tvDisplay = findViewById(R.id.tvActDisplay);
        ivClose = findViewById(R.id.ivActClose);

        rgGender = findViewById(R.id.rgActGender);
        rbMale = findViewById(R.id.rbActMale);
        rbFemale = findViewById(R.id.rbActFemale);

        chbCricket = findViewById(R.id.chbActCricket);
        chbFootball = findViewById(R.id.chbActFootball);
        chbHockey = findViewById(R.id.chbActHockey);

        ivName = findViewById(R.id.ivActClrFirstName);
        ivPhone = findViewById(R.id.ivActClrPhoneNumber);
        ivEmail = findViewById(R.id.ivActClrEmail);

        etPhoneNumber.setText("+91");

        getSupportActionBar().setTitle("My Activity");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.dashboard_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.new_game) {

        } else if (item.getItemId() == R.id.help) {

        }else if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    void setTypefaceOnView() {

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/BalsamiqSans-Bold.ttf");

        btnSubmit.setTypeface(typeface);
        tvDisplay.setTypeface(typeface);
        etFirstName.setTypeface(typeface);
        etPhoneNumber.setTypeface(typeface);
        etEmailAddress.setTypeface(typeface);

        //etLastName.setTypeface(typeface);
    }


    void intViewEvent() {
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isValid()){


                    String name = etFirstName.getText().toString();
                    String phoneNumber = etPhoneNumber.getText().toString();
                    String email = etEmailAddress.getText().toString();
                    String gender = rbMale.isChecked() ? rbMale.getText().toString() : rbFemale.getText().toString();
                    String hobbies = "" ;
                    if (chbCricket.isChecked()) {
                        hobbies += "," + chbCricket.getText().toString();
                    }
                    if (chbFootball.isChecked()) {
                        hobbies += "," + chbFootball.getText().toString();
                    }
                    if (chbHockey.isChecked()) {
                        hobbies += "," + chbHockey.getText().toString();
                    }
                    TblUserData tblUserData = new TblUserData(MainActivity.this);
                    long lastInsertedId = tblUserData.insertUserDetail(name, phoneNumber, email, gender, hobbies);
                    Toast.makeText(getApplicationContext(), lastInsertedId > 0 ?
                            "User Inserted Successfully" : "Something went wrong", Toast.LENGTH_SHORT).show();

                }
                /*HashMap<String, Object> map = new HashMap();
                map.put(Const.FIRST_NAME, etFirstName.getText().toString());
                //map.put(Const.LAST_NAME, etLastName.getText().toString());
                map.put(Const.PHONE_NUMBER, etPhoneNumber.getText().toString());
                map.put(Const.EMAIL_ADDRESS, etEmailAddress.getText().toString());
                map.put(Const.GENDER, rbMale.isChecked() ? rbMale.getText().toString() : rbFemale.getText().toString());

                String hobbies = "";
                if (chbCricket.isChecked()) {
                    hobbies += "," + chbCricket.getText().toString();
                }
                if (chbFootball.isChecked()) {
                    hobbies += "," + chbFootball.getText().toString();
                }
                if (chbHockey.isChecked()) {
                    hobbies += "," + chbHockey.getText().toString();
                }
                map.put(Const.HOBBY, hobbies);
                userList.add(map);

                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                intent.putExtra("UserList", userList);
                startActivity(intent);*/
               /* String firstName = etFirstName.getText().toString();
                String lastName = etPhoneNumber.getText().toString();
                String temp = firstName + " " + lastName;*/

                  /* // Explisit intent
                   Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                    String concatTempString = etFirstName.getText().toString() + " "+ etLastName.getText().toString();
                   intent.putExtra("finalValue", concatTempString);
                    startActivity(intent);
                    tvDisplay.setText(concatTempString);*/

                //Toast.makeText(getApplicationContext(), rbMale.isChecked() ? "Male" : "Female", Toast.LENGTH_LONG).show();

                etFirstName.getText().clear();
                etEmailAddress.getText().clear();
                etPhoneNumber.getText().clear();

                etPhoneNumber.setText("+91");

            }
        });

        //clear name on image click
        ivName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etFirstName.getText().clear();
            }
        });

        //clear email on image click
        ivEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etEmailAddress.getText().clear();
            }
        });

        //clear phone number on image click
        ivPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etPhoneNumber.getText().clear();
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (R.id.rbActFemale == rgGender.getCheckedRadioButtonId()){
            chbCricket.setVisibility(View.VISIBLE);
            chbFootball.setVisibility(View.VISIBLE);
            chbHockey.setVisibility(View.GONE);
        }

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.rbActFemale) {
                    chbCricket.setVisibility(View.VISIBLE);
                    chbFootball.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.GONE);
                } else if (i == R.id.rbActMale) {
                    chbCricket.setVisibility(View.VISIBLE);
                    chbFootball.setVisibility(View.VISIBLE);
                    chbHockey.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    boolean isValid() {
        boolean flag = true;
        //Name Validation
        if (TextUtils.isEmpty(etFirstName.getText())) {
            etFirstName.setError(getString(R.string.error_enter_value));
            etFirstName.requestFocus();
            flag = false;
        } else {
            if (etFirstName.getText().toString().contains(" ")) {
                etFirstName.setError("No Spaces Allowed");
                flag = false;
            }
        }
        //Phone Number Validation
        if (TextUtils.isEmpty(etPhoneNumber.getText())) {
            etPhoneNumber.setError("Enter Number");
            etPhoneNumber.requestFocus();
            flag = false;
        }else {
            String phoneNumber = etPhoneNumber.getText().toString();
            if (phoneNumber.length()<10){
                etPhoneNumber.setError("Enter Valid Number");
                flag = false;
            }
        }
        //Email Validation
        if (TextUtils.isEmpty(etEmailAddress.getText())) {
            etEmailAddress.setError(getString(R.string.error_enter_value));
            flag = false;
        }else{
            String email = etEmailAddress.getText().toString().trim();
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if (!email.matches(emailPattern)) {
                etEmailAddress.setError("Enter Valid Email Address");
                flag = false;
            }
        }
        //Checkbox Validation
        if (!(chbCricket.isChecked() && chbCricket.isChecked() && chbCricket.isChecked())) {
            Toast.makeText(this, "Please Select any one Checkbox", Toast.LENGTH_LONG).show();
            flag = false;
        }

        return flag;
    }
}
